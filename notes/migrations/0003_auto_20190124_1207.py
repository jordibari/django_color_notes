# Generated by Django 2.1.5 on 2019-01-24 12:07

import autoslug.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notes', '0002_auto_20190124_1149'),
    ]

    operations = [
        migrations.AddField(
            model_name='color',
            name='slug',
            field=autoslug.fields.AutoSlugField(default='a', editable=False, populate_from='name', unique=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='note',
            name='slug',
            field=autoslug.fields.AutoSlugField(default='a', editable=False, populate_from='name', unique=True),
            preserve_default=False,
        ),
    ]
