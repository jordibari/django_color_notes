from django.contrib.auth.models import User
from rest_framework import serializers
from notes.models import Color, Note, Board

# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff')

class ColorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Color
        fields = ('url', 'r', 'g', 'b', 'name', 'slug')

class BoardSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Board
        fields = ('url', 'name', 'color', 'slug', 'created_at')

class NoteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Note
        fields = ('url', 'text', 'color', 'board', 'slug', 'created_at')
        