from django.contrib import admin
from notes.models import Color, Note, Board

# Register your models here.

class ColorAdmin(admin.ModelAdmin):
    list_display = ('name',
                    'slug')
admin.site.register(Color, ColorAdmin)

class ColorInline(admin.TabularInline):
    model = Color
  
class NoteAdmin(admin.ModelAdmin):
    list_display = ('text',
                    'color',
                    'slug',
                    'created_at')
    # inlines = [
    #     ColorInline,
    # ]
admin.site.register(Note, NoteAdmin)

class NoteInline(admin.TabularInline):
    model = Note
    
class BoardAdmin(admin.ModelAdmin):
    list_display = ('name',
                    'slug',
                    'created_at')
    inlines = [
        NoteInline,
    ]
    
admin.site.register(Board, BoardAdmin)