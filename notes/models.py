from autoslug import AutoSlugField
from django.db import models
from django.urls import reverse

# Create your models here.


class Color(models.Model):
    name = models.CharField(max_length=64, null=True)
    r = models.CharField(max_length=2, null=False)
    g = models.CharField(max_length=2, null=False)
    b = models.CharField(max_length=2, null=False)
    slug = AutoSlugField(unique=True, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    
    def hex(self):
        return '0x' + self.r + self.g + self.b
        
    def __str__(self):
        return self.name or self.r + self.g + self.b

    
class Board(models.Model):
    name = models.CharField(max_length=200, verbose_name='name', null=False)
    color = models.ForeignKey(Color, on_delete=models.PROTECT, null=False, verbose_name='Board background color')
    slug = AutoSlugField(unique=True, populate_from='name', null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.slug
    
    def get_absolute_url(self):
        return reverse('board', kwargs={'slug': self.slug})
    
class Note(models.Model):
    text = models.CharField(max_length=200, verbose_name='Text Content')
    color = models.ForeignKey(Color, on_delete=models.PROTECT, null=False, verbose_name='Note background color')
    board = models.ForeignKey(Board, on_delete=models.CASCADE, null=False)
    slug = AutoSlugField(unique_with='board__slug', null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.board.slug + ':' + str(self.created_at)