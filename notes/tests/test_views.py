from django.test import TestCase
from notes.models import Board, Color
from django.urls import reverse

class IndexViewTestCase(TestCase):
    def test_get_the_index(self):
        url = reverse('index')
        response = self.client.get(url)
        assert response.status_code == 200
        
    def test_home_has_several_boards(self):
        board_color = Color.objects.create(name='white',r='FF', g='FF', b='FF' )
        board1 = Board.objects.create(name='Test Board', color=board_color)
        board2 = Board.objects.create(name='Test Board', color=board_color)
        board3 = Board.objects.create(name='Test Board', color=board_color)
        url = reverse('index')
        response = self.client.get(url)
        assert response.status_code == 200
        boards = response.context['boards']
        assert len(boards) == 3
        assert board1 in boards


class BoardViewTestCase(TestCase):
    def test_get_absolute_url(self):
        board_color = Color.objects.create(name='white',r='FF', g='FF', b='FF' )
        board = Board.objects.create(name='Test Board', color=board_color)
        url = reverse('board', kwargs={'slug': board.slug})
        assert board.get_absolute_url() == url
        response = self.client.get(url)
        assert response.status_code == 200