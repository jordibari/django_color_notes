from django.test import TestCase
from notes.models import Color, Note, Board

class TestModels(TestCase):
    
    def test_instanciate_color_without_name(self):
        color = Color.objects.create(r='FF', g='FF', b='FF' )
        assert color.r == 'FF'
        assert color.g == 'FF'
        assert color.b == 'FF'
        assert color.hex() == '0xFFFFFF'
        assert str(color) == 'FFFFFF'    
    
    def test_instanciate_color_with_name(self):
        color = Color.objects.create(name='white',r='FF', g='FF', b='FF' )
        assert color.r == 'FF'
        assert color.g == 'FF'
        assert color.b == 'FF'
        assert color.hex() == '0xFFFFFF'
        assert str(color) == 'white'

    def test_instanciate_note(self):
        board_color = Color.objects.create(name='white',r='FF', g='FF', b='FF' )
        board = Board.objects.create(name='Test Board', color=board_color)
        bg_color = Color.objects.create(name='white',r='FF', g='FF', b='FF' )
        note = Note.objects.create(text=u'Fiera', color=bg_color, board=board)
        assert note.text == 'Fiera'
        assert str(note.color) == 'white'
        
    def test_instanciate_board(self):
        board_color = Color.objects.create(name='white',r='FF', g='FF', b='FF' )
        board = Board.objects.create(name='Test Board', color=board_color)
        
    def test_board_can_have_many_notes(self):
        board_color = Color.objects.create(name='white',r='FF', g='FF', b='FF' )
        board = Board.objects.create(name='Test Board', color=board_color)
        bg_color = Color.objects.create(name='white',r='FF', g='FF', b='FF' )
        note_1 = Note.objects.create(text=u'Fiera', color=bg_color, board=board)
        note_2 = Note.objects.create(text=u'Fiera', color=bg_color, board=board)
        assert board.note_set.count() == 2
