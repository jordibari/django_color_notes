from django.urls import path, reverse, include
from django.views.generic.base import TemplateView
from notes.views import IndexView, UserViewSet, ColorViewSet, NoteViewSet, BoardViewSet
from rest_framework import routers

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'api/users', UserViewSet)
router.register(r'api/colors', ColorViewSet)
router.register(r'api/notes', NoteViewSet)
router.register(r'api/boards', BoardViewSet)


urlpatterns = [
    path('home', IndexView.as_view(), name='index'),
    path('board/<str:slug>/', TemplateView.as_view(template_name='board.html'), name='board'),
    path(r'', include(router.urls)),
    path(r'api/', include('rest_framework.urls', namespace='rest_framework'))
]
