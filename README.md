
     ,-----.,--.                  ,--. ,---.   ,--.,------.  ,------.
    '  .--./|  | ,---. ,--.,--. ,-|  || o   \  |  ||  .-.  \ |  .---'
    |  |    |  || .-. ||  ||  |' .-. |`..'  |  |  ||  |  \  :|  `--, 
    '  '--'\|  |' '-' ''  ''  '\ `-' | .'  /   |  ||  '--'  /|  `---.
     `-----'`--' `---'  `----'  `---'  `--'    `--'`-------' `------'
    ----------------------------------------------------------------- 
    
sudo apt-get install build-essential

sudo apt-get install libssl-dev libsqlite3-dev libgdbm-dev libc6-dev libbz2-dev

sudo apt install libssl-dev libncurses5-dev libsqlite3-dev libreadline-dev libtk8.5 libgdm-dev libdb4o-cil-dev libpcap-dev

cd /usr/src

sudo wget https://www.python.org/ftp/python/3.7.1/Python-3.7.1.tgz

sudo tar xzf Python-3.7.1.tgz

cd Python-3.7.1

sudo ./configure --enable-optimizations

sudo make altinstall

python3.7 -V

sudo pip install virtualenv

sudo pip install virtualenvwrapper /sudo pip install virtualenvwrapper  --ignore-installed six 
   
source /usr/local/bin/virtualenvwrapper.sh
   
mkvirtualenv <env_name> -a $(pwd) --python=$(which python3.7)
   
workon <env_name>

python manage.py createsuperuser
Username (leave blank to use 'ubuntu'): jordi
Email address: jordi@ciudadaniai.org
Password: aurelia



Welcome to your Django project on Cloud9 IDE!

Your Django project is already fully setup. Just click the "Run" button to start
the application. On first run you will be asked to create an admin user. You can
access your application from 'https://django-colors-jbci.c9users.io/' and the admin page from 
'https://django-colors-jbci.c9users.io/admin'.

## Starting from the Terminal

In case you want to run your Django application from the terminal just run:

1) Run syncdb command to sync models to database and create Django's default superuser and auth system

    $ python manage.py migrate

2) Run Django

    $ python manage.py runserver $IP:$PORT
    
## Configuration

You can configure your Python version and `PYTHONPATH` used in
Cloud9 > Preferences > Project Settings > Language Support.

## Support & Documentation

Django docs can be found at https://www.djangoproject.com/

You may also want to follow the Django tutorial to create your first application:
https://docs.djangoproject.com/en/1.9/intro/tutorial01/

Visit http://docs.c9.io for support, or to learn more about using Cloud9 IDE.
To watch some training videos, visit http://www.youtube.com/user/c9ide